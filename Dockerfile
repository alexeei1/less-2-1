FROM python:latest
RUN apt update && apt install -y pip
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN pip install -r requirements.txt 

